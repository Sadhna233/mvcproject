﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserDAL
{
    public class Dbconnection
    {


        private static string conString = ConfigurationManager.ConnectionStrings["Dbcon"].ConnectionString;

        //for Sql connection
        private SqlConnection con = new SqlConnection(conString);

        public SqlConnection GetSqlConnection()
        {
            return con;
        }

        //open db connection

        public void OpenConnection()
        {
            if (con.State == System.Data.ConnectionState.Closed)
            {
                con.Open();
            }

        }

        // close Db Connection

        public void CloseConnection()
        {
            if (con.State == System.Data.ConnectionState.Open)
            {
                con.Close();
            }
        }
    }
}
﻿using MVCproject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCproject.Controllers
{

    public class UserController : Controller
    {
        // GET: User

        ModelMgr modelMgr = new ModelMgr();

        public ActionResult SignUp()
        {
            return View();
        }
        public ActionResult SignIn()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SignUp(UserModel  userModel)
        {
            try
            {
                if (modelMgr.AddUser(userModel))
                {
                    ViewBag.message = "Loged in successfully";

                }
            }
          catch(Exception ex)
            {

                ViewBag.message = ex.Message;
            }
            return View();

        }
        [HttpPost]
        public ActionResult SignIn(UserModel userModel)
        {
            try
            {
                if (modelMgr.UserLogin(userModel))
                {
                    ViewBag.message = "Loged in successfully";
                   return RedirectToAction("Index", "Home");

                }
            }
            catch (Exception ex)
            {

                ViewBag.message = ex.Message;
            }
            return View();



        }


        public ActionResult Display()
        {
            return View();
        }
    }
}
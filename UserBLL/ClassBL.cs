﻿using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserDAL;

namespace UserBLL
{
    public class ClassBL
    {
        ClassDL classDl = new ClassDL();
        public bool AddUser(User user)
        {
            try {

                if (classDl.AddUser(user))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return false;
            }

        public User UserLogin(User user)
        {
            User user1 = new User();
            user1 = classDl.UserLogin(user);
           
            try
            {

                if (user1.name != null)
                {
                    return user1;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            throw new Exception("Details not found");
        }

    }
    
}

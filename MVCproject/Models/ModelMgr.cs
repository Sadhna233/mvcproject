﻿using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UserBLL;

namespace MVCproject.Models
{
    public class ModelMgr
    {
        ClassBL classBL = new ClassBL();
       
        public  bool AddUser(UserModel userModel)
        {
            User user = new User();
            user.userid = userModel.userid;
            user.name = userModel.name;
            user.mobileNo = userModel.mobileNo;
            user.Email = userModel.Email;
            user.password = userModel.password;


            try
            {

                if (classBL.AddUser(user))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return false;
        }

       public bool UserLogin(UserModel userModel)
        {
            
                User user = new User();
                user.userid = userModel.userid;
            user.password = userModel.password;


            try
            {
                UserModel userModel1 = new UserModel();
                User userresult = new User();
                userresult = classBL.UserLogin(user);

                if (userresult.userid==user.userid)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return false;
        }
    }
    
}
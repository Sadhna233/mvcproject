﻿using Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserDAL
{
    public class ClassDL
    {
        Dbconnection dbconnection = new Dbconnection();
        public bool AddUser(User user)
        {


            try
            {


                SqlCommand command = new SqlCommand("insertuserdetails", dbconnection.GetSqlConnection());
                command.CommandType = CommandType.StoredProcedure;

                //  @userid,@uname,@psw,@mobileno,@email
                command.Parameters.AddWithValue("@userid", user.userid);
                command.Parameters.AddWithValue("@uname", user.name);
                command.Parameters.AddWithValue("@psw", user.password);
                command.Parameters.AddWithValue("@mobileno", user.mobileNo);
                command.Parameters.AddWithValue("@email", user.Email);

                dbconnection.OpenConnection();

                command.ExecuteNonQuery();
                dbconnection.CloseConnection();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);

            }

            return true;
        }

        public User UserLogin(User user)
        {
            User user1 = new User();
            try
            {


                SqlCommand command = new SqlCommand("FetchDetails", dbconnection.GetSqlConnection());
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@userid", user.userid);
                command.Parameters.AddWithValue("@psw", user.password);

                dbconnection.OpenConnection();

                SqlDataReader userData = command.ExecuteReader();
                while (userData.Read())
                {

                    user1.userid = userData.GetInt32(0);
                    user1.name = userData.GetString(1);


                    user1.password = userData.GetString(2);
                    user1.mobileNo = userData.GetString(3);
                    user1.Email = userData.GetString(4);


                }
                
                dbconnection.CloseConnection();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);

            }

            return user1;
        }
    }
}